<section class="section">
	<div class="container">
		<div class="row">
			<div class="col-lg-7">
				<div class="cta-content">
					<div class="divider mb-4"></div>
					<h2 class="mb-5 text-lg">Nous sommes heureux de vous offrir <span class="title-color">la chance d'être en bonne santé</span></h2>
				</div>
			</div>
		</div>
	</div>
</section>
