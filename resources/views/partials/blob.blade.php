<svg viewBox="0 0 200 200" xmlns="http://www.w3.org/2000/svg">
    <defs>
        <clipPath id="user-space" clipPathUnits="userSpaceOnUse">
            <path fill="#FF0066" d="M34.6,-23.1C44.8,-2.4,52.9,16.4,46.7,35.4C40.6,54.4,20.3,73.5,-0.7,73.9C-21.7,74.3,-43.3,55.9,-50,36.6C-56.7,17.3,-48.5,-3,-37.7,-24.1C-26.8,-45.2,-13.4,-67.1,-0.6,-66.7C12.2,-66.4,24.5,-43.8,34.6,-23.1Z" transform="translate(100 100)" />
        </clipPath>
      </defs>
      <image width="100%" height="100%" preserveAspectRatio="xMinYMin slice" xlink:href="{{$url}}" clip-path="url(#user-space)" />
  </svg>
